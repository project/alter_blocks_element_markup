# Alter blocks element markup

Markup modifier for blocks titles tags.

## INTRODUCTION

This module allows a site builder to change block level element tags or add classes within the block configurations.

It accounts for both content and configuration blocks that are rendered using the block.html.twig template.

By default, the label is a <p> tag with attributes role="heading" and aria-level="2". 
It is overwritable using "Fences" module for instance.

* For a full description of the module, visit the project page: 
  https://www.drupal.org/project/alter_blocks_element_markup
* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/alter_blocks_element_markup
  
## REQUIREMENTS

This module requires no modules outside of Drupal core.
  
## RECOMMENDED MODULES

Fences (https://www.drupal.org/project/fences)

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

## CONFIGURATION

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

## MAINTAINERS

This project has been sponsored by:

[Insign](https://www.drupal.org/insign)


