<?php

namespace Drupal\alter_blocks_element_markup;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\block\Entity\Block;

/**
 * Define the AlterBlocksElementMarkup block class.
 */
class AlterBlocksElementMarkup {

  /**
   * alter_blocks_element_markup block preprocess.
   *
   * @param array &$variables
   *   An array of the block template variables.
   */
  public static function preprocess(array &$variables) {
    $element = &$variables['elements'];

    if (!isset($element['#id'])) {
      return;
    }
    $block = Block::load($element['#id']);

    if ($block instanceof ThirdPartySettingsInterface) {
      $settings = $block->getThirdPartySetting('alter_blocks_element_markup', 'alter_blocks_element_markup', []);
    }


    // Iterate over alter_blocks_element_markup sections and set the classes and/or HTML element.
    if (isset($settings['sections'])) {
      foreach ($settings['sections'] as $section => $info) {
        $classes = self::processClasses($info['classes']);

        // Set the alter_blocks_element_markup section HTML element classes.
        switch ($section) {
          case 'wrapper':
            self::addClasses($variables, 'attributes', $classes);
            break;

          case 'label':
            self::addClasses($variables, 'title_attributes', $classes);
            break;

          case 'content':
            self::addClasses($variables, 'content_attributes', $classes);
            break;
        }

        // Set the alter_blocks_element_markup section HTML element.
        $variables['alter_blocks_element_markup_element'][$section] = $info['element'];
      }
    }
  }

  /**
   * Add alter_blocks_element_markup block configuration form.
   *
   * @param array &$form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function addConfigForm(array &$form, FormStateInterface $form_state) {
    $entity = $form_state->getFormObject()->getEntity();

    if (!$entity instanceof ThirdPartySettingsInterface) {
      return;
    }
    $form['settings']['alter_blocks_element_markup'] = [
      '#type' => 'alter_blocks_element_markup_tag',
      '#title' => new TranslatableMarkup('Alter blocks element markup'),
      '#sections' => [
        'wrapper' => new TranslatableMarkup('Wrapper'),
        'label' => new TranslatableMarkup('Label'),
        'content' => new TranslatableMarkup('Content'),
      ],
      '#default_value' => $entity->getThirdPartySetting(
        'alter_blocks_element_markup', 'alter_blocks_element_markup', []
      ),
    ];

    $form['#entity_builders'][] = [get_class(), 'buildBlockEntity'];

  }

  /**
   * Block entity builder callback.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param \Drupal\Core\Entity\Block $entity
   *   The entity object.
   * @param array &$form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function buildBlockEntity(
    $entity_type_id,
    Block $entity,
    array &$form,
    FormStateInterface &$form_state
  ) {
    $entity
      ->setThirdPartySetting('alter_blocks_element_markup', 'alter_blocks_element_markup', $form_state->getValue(['settings', 'alter_blocks_element_markup']))
      ->save();
  }

  /**
   * Process CSS classes.
   *
   * @param array|string $classes
   *   Either a string or array of classes.
   * @param string $delimiter
   *   The boundary string for exploding classes.
   *
   * @return array
   *   An array of processed CSS classes.
   */
  protected static function processClasses($classes, $delimiter = ', ') {
    if (!is_array($classes)) {
      $classes = explode($delimiter, $classes);
    }

    return array_map(
      '\Drupal\Component\Utility\Html::cleanCssIdentifier', $classes
    );
  }

  /**
   * Add classes to attributes.
   *
   * @param array &$variables
   *   An array of template variables.
   * @param string $name
   *   The class attributes key.
   * @param array $classes
   *   An array of classes to add.
   */
  protected static function addClasses(&$variables, $name, array $classes) {
    if (!isset($variables[$name]['class'])) {
      $variables[$name]['class'] = [];
    }

    $variables[$name]['class'] = array_merge($variables[$name]['class'], $classes);
  }

}
