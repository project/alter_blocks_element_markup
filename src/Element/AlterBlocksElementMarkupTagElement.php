<?php

namespace Drupal\alter_blocks_element_markup\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Define the Alter Blocks Element Markup tag form element.
 *
 * @FormElement("alter_blocks_element_markup_tag")
 */
class AlterBlocksElementMarkupTagElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#tree' => TRUE,
      '#input' => TRUE,
      '#title' => new TranslatableMarkup('Alter Blocks Element Markup'),
      '#sections' => [],
      '#process' => [
        [$class, 'alterBlocksElementMarkupTagElementProcess'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      return isset($element['#default_value'])
        && is_array($element['#default_value']) ? $element['#default_value'] : [];
    }

    if (!is_array($input)) {
      return [];
    }

    return $input;
  }

  /**
   * Process Alter Blocks Element Markup tag element.
   *
   * @param array &$element
   *   An array of the element properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Drupal form state object.
   * @param array &$complete_form
   *   An array of the completed form elements.
   *
   * @return array
   *   An array of the fully processed element.
   */
  public static function alterBlocksElementMarkupTagElementProcess(
    array &$element,
    FormStateInterface $form_state,
    array &$complete_form
  ) {

    $element['sections'] = [
      '#type' => 'details',
      '#title' => $element['#title'],
      '#open' => FALSE,
    ];
    $value = is_array($element['#value']) ? $element['#value'] : [];

    foreach ($element['#sections'] as $section => $label) {
      $element['sections'][$section]['element'] = [
        '#type' => 'textfield',
        '#title' => new TranslatableMarkup('@label element', ['@label' => $label]),
        '#default_value' => isset($value['sections'][$section]['element']) ? $value['sections'][$section]['element'] : NULL,
      ];
      $element['sections'][$section]['classes'] = [
        '#type' => 'textfield',
        '#title' => new TranslatableMarkup('@label classes', ['@label' => $label]),
        '#description' => new TranslatableMarkup('Input multiple class values using a comma delimiter.'),
        '#default_value' => isset($value['sections'][$section]['classes']) ? $value['sections'][$section]['classes'] : NULL,
        '#states' => [
          'visible' => [
            ':input[name="' . $element['#name'] . '[sections][' . $section . '][element]"]' => ['!value' => 'none'],
          ],
        ],
      ];
    }

    return $element;
  }

}
